import React from "react";

export default class TextArea extends React.Component {

    constructor(props) {
        super(props);
        this.state = {value: ''};

        this.handleOriginalChange = this.handleOriginalChange.bind(this);
    }

    handleOriginalChange(event) {
        this.setState({value: event.target.value});
    }

    render(){
        return(
            <div className="p-2 w-full">
                <div className="relative">
                    <label htmlFor="message" className="leading-7 text-sm text-gray-600">Оригинальный текст</label>
                    <textarea id="message" name="message"
                              onChange={this.handleOriginalChange}
                              className="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-indigo-500 focus:bg-white focus:ring-2 focus:ring-indigo-200 h-32 text-base outline-none text-gray-700 py-1 px-3 resize-none leading-6 transition-colors duration-200 ease-in-out"></textarea>
                </div>

                <div className="relative">
                    <label htmlFor="message" className="leading-7 text-sm text-gray-600">Чморизированный текст</label>
                    <textarea id="message" name="message"
                              readOnly={true}
                              value={(this.state.value).replace(/м./gi, 'чм')}
                              className="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-indigo-500 focus:bg-white focus:ring-2 focus:ring-indigo-200 h-32 text-base outline-none text-gray-700 py-1 px-3 resize-none leading-6 transition-colors duration-200 ease-in-out"></textarea>
                </div>

            </div>
        );
    }
}