import React from "react";
import TextArea from "./Textarea/TextArea";

export default class Chmorisator extends React.Component {

    render(){
        return(
            <section className="text-gray-600 body-font relative">
                <div className="container px-5 py-24 mx-auto">

                    <div className="flex flex-col text-center w-full">
                        <h1 className="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">Чморизатор</h1>
                        <p className="lg:w-2/3 mx-auto leading-relaxed text-base">Легко чморизируем для вас всё, что угодно!</p>
                    </div>

                    <div className="lg:w-1/2 md:w-2/3 mx-auto">
                        <div className="flex flex-wrap -m-2">
                            <TextArea/>
                        </div>
                    </div>

                </div>
            </section>
        );
    }
}